function get_radar() {
    var config = {
        type: 'radar',
        data: {
            datasets: [{
                // We will add the data later
                data: [],
            }],
            // We are adding the labels when we add the data
            labels: ["Miner", "Woodworker", "Stoneworker", "Ranger", "Doctor", "Military",
            "Farmer","Fishery worker","Metalsmith","Jeweler","Craftsdwarf","Engineer",
            "Administrator","Broker","Miscellaneous","Performance","Scholar"]
        },
        options: {
            title: {
                display: true,
                text: 'Skill focus',
                fontColor: 'white',
            },
            maintainAspectRatio: true,
            spanGaps: false,
            elements: {
                line: {
                    tension: 0.000001
                }
            },
            plugins: {
                filler: {
                    propagate: false
                },
                'samples-filler-analyser': {
                    target: 'chart-analyser'
                }
            },
            borderColor: '#1A2D37',
        },
    };
    return config;
}

function create_radar_chart(){
    // Get the `skill_radar` id from the html page.
    var ctx = document.getElementById('skill_radar').getContext('2d');
    // This is only use to set the ratio (width-height)
    ctx.canvas.width = 300;
    ctx.canvas.height = 300;
    // Get default settings for our plot
    chart1_data = get_radar();

    // Create the graph
    window.skill_radar = new Chart(ctx, chart1_data);
}

function fill_skill_radar(data){
    window.skill_radar.config.data.datasets = [{
        // backgroundColor: "#39a0ed",
        borderColor: '#39a0ed',
        data: construct_skill_group_data(data),
        label: 'Level'
    }];
    window.skill_radar.update();
}