function get_doughnut() {
    var config = {
        type: 'doughnut',
        data: {
            datasets: [{
                // We will add the data later
                data: [],
                backgroundColor: [
                    '#9ec9d9','#ffff00','#ffffff','#008000',
                    '#800080','#000000','#808000','#000080',
                    '#808080','#00ff00','#0000ff','#ff0000',
                    '#800080','#008080','#008080','#008080',
                    '#008080',
                ],
            }],
            labels: ["Miner", "Woodworker", "Stoneworker", "Ranger", "Doctor", "Military",
            "Farmer","Fishery worker","Metalsmith","Jeweler","Craftsdwarf","Engineer",
            "Administrator","Broker","Miscellaneous","Performance","Scholar"]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Skill focus',
                fontColor: 'white',
            },
            legend: {
                display: false,
                position: 'top',
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        },
    };
    return config;
}

function create_doughnut_chart(){
    // Get the `skill_doughnut` id from the html page.
    var ctx = document.getElementById('skill_doughnut').getContext('2d');
    // This is only use to set the ratio (width-height)
    ctx.canvas.width = 300;
    ctx.canvas.height = 300;
    // Get default settings for our plot
    chart1_data = get_doughnut();

    // Create the graph
    window.skill_doughnut = new Chart(ctx, chart1_data);
}

function fill_skill_doughnut(data){
    window.skill_doughnut.config.data.datasets[0].data = construct_skill_group_data(data);
    window.skill_doughnut.update();
}