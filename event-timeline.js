
// Some global variables
var max_year = 0;

// Our default pie chart plot settings
// You can find more info and options here: https://www.chartjs.org/docs/latest/
// There are a lot more types of graphs: https://www.chartjs.org/samples/latest/
function get_timeline() {
    var config = {
        type: 'line',
        data: {
            datasets: [{
                // We will add the data later
                data: [],
                backgroundColor: '#39a0ed',
                borderColor: '#39a0ed',
                label: 'Amount of events',
                type: 'line',
                pointRadius: 0,
                fill: false,
                lineTension: 0,
                borderWidth: 2
            }],
            // We are adding the labels when we add the data
            labels: []
        },
        options: {
            responsive: true,
            legend: {
                labels: {
                    fontColor: 'white',
                }
            },
            title: {
                display: true,
                text: 'Timeline',
                fontColor: 'white',
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Years',
                        fontColor: 'white',
                    },
                    ticks: {
                        fontColor: 'white',
                    },
                    gridLines: {
                        color: '#1A2D37',
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Amount of Events',
                        fontColor: 'white',
                    },
                    ticks: {
                        fontColor: 'white',
                    },
                    gridLines: {
                        color: '#1A2D37',
                    }
                }]
            },
        },
        verticalLines: [{
            x: 10,
            text: "Sample test",
            color: "#ff0000",
        }]
    };
    return config;
}

function create_chart(){
    // Get the `event_timeline` id from the html page.
    var ctx = document.getElementById('event_timeline').getContext('2d');
    // This is only use to set the ratio (width-height)
    ctx.canvas.width = 500;
    ctx.canvas.height = 300;
    // Get default settings for our plot
    chart1_data = get_timeline();

    // Create the graph
    window.timeline = new Chart(ctx, chart1_data);
}

function parse_data(data){
    data = data.data;
    
    // Update `max_year`
    var years_in_data = groupBy(data, event => event.year);
    years_in_data = Array.from(years_in_data).map(year_group => {
        return {
            year: year_group[0],
            count: year_group[1].length,
        }
    });
    max_year = Math.max(...years_in_data.map(obj => obj.year));
    max_year += 10;
    // Create a timeline and fill in the data that is provided.
    // If data is missing it will just leave it undefined.
    // The graph will have a gap in it where the data is missing.
    var full_timeline = Array(max_year).fill(0);
    for (i = 0; i < years_in_data.length; i++) {
        var year = years_in_data[i].year;
        var count = years_in_data[i].count;
        if( year < full_timeline.length ){
            full_timeline[year] = count;
        }
    }
    return full_timeline;
}

function load_events_timeline(hf_id) {
    // Get the data from the URL and process it
    fetch(`${base_url}/api/link_he_hf/${hf_id}`)
        .then(response => response.json())
        // Once converted to json, lets do something with the data
        .then(data => {
            var full_timeline = parse_data(data);

            var dataset = window.timeline.config.data.datasets[0];
            dataset.data = full_timeline;
            
            // Create a list of years starting from 0 to `max_year`.
            // Example: [0,1,2,3,4,5,6,7,8,9,10,11,...]
            window.timeline.config.data.labels = [...Array(max_year).keys()];
            window.timeline.update();
        });
};